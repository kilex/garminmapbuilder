#!/bin/bash
cd $1
MAPNAME=`../../mapname.sh`
echo $MAPNAME
../../shell1.sh | xargs ../../python.py
tar -czvf "$MAPNAME.tar.gz" "$MAPNAME.gmapi"
mv "$MAPNAME.tar.gz" ../../result_tmp/
cd ..
