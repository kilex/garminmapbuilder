# Docker GARMIN maps for macos
Предназначен для генерации карт для GARMIN MapInstall на основании данных OSM от [gis-lab](http://garmin.gis-lab.info)

для работы необходим [docker](https://store.docker.com/search?type=edition&offering=community), wget. 

## запуск
редактируем файл make.sh, убираем не нужные регионы, затем запускаем

```
./make.sh
```

в папке report появятся готовые архивы с картами