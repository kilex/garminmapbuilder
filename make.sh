#!/bin/bash
mkdir -p download
wget http://garmin.gis-lab.info/files/ru.pfo.7z --continue -O ./download/ru.pfo.7z
wget http://garmin.gis-lab.info/files/ru.urfo.7z --continue -O ./download/ru.urfo.7z
wget http://garmin.gis-lab.info/files/ru.cfo.7z --continue -O ./download/ru.cfo.7z
wget http://garmin.gis-lab.info/files/ru.ufo.7z --continue -O ./download/ru.ufo.7z
wget http://garmin.gis-lab.info/files/ru.sfo.7z --continue -O ./download/ru.sfo.7z
wget http://garmin.gis-lab.info/files/ru.skfo.7z --continue -O ./download/ru.skfo.7z
wget http://garmin.gis-lab.info/files/ru.szfo.7z --continue -O ./download/ru.szfo.7z
wget http://garmin.gis-lab.info/files/ru.dfo.7z --continue -O ./download/ru.dfo.7z
wget http://garmin.gis-lab.info/files/russia.7z --continue -O ./download/russia.7z
docker build -t map .
docker run -v $(pwd)/result:/mapapp/result map
