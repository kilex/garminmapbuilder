FROM crazymax/7zip as xz
WORKDIR /usr/src/app
COPY  ./download .
RUN find -name "*.7z" | xargs -n1 7za x
RUN rm *.7z

FROM python:3
WORKDIR /mapapp
COPY --from=xz /usr/src/app ./map/
COPY ./scripts/ .
RUN ./start.sh
CMD ./copy.sh
